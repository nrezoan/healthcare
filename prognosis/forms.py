from django import forms


class AttributeForm(forms.Form):
    Age = forms.IntegerField()
    Sex = forms.BooleanField()
    CP = forms.IntegerField()
    Cholestorel = forms.IntegerField()
    Thalach = forms.IntegerField()
    CA = forms.IntegerField()
