from django.contrib import admin

# Register your models here.
from prognosis.models import AttributeTable

admin.site.register(AttributeTable)
