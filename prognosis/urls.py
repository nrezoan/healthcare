from django.contrib import admin
from django.urls import path

from prognosis import views

urlpatterns = [
    path('', views.attribute, name='attribute'),
    path('test/', views.test),
]
