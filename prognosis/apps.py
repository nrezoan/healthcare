from django.apps import AppConfig


class PrognosisConfig(AppConfig):
    name = 'prognosis'
