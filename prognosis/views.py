from django.http import HttpResponse

from django.shortcuts import render
from django.template import loader
from django.utils import http

from prognosis.models import AttributeTable
from .forms import AttributeForm

import numpy as np
from sklearn import preprocessing, cross_validation, neighbors, svm
import pandas as pd


# Create your views here.


def attribute(request):
    val = test();
    form = AttributeForm(request.POST or None)
    if form.is_valid():
        for value in form.cleaned_data.values():
            print(value)
        form = AttributeForm()
    template = loader.get_template('prognosis/forms.html')
    context = {
        "form": form,
    }

    return HttpResponse(template.render(context, request))


def test(request):
    all_values = AttributeTable.objects.all().values()
    df = pd.DataFrame(list(all_values))
    df.replace('?', -99999, inplace=True)
    df.drop(['id'], 1, inplace=True)
    df = df[['age', 'sex', 'cp', 'chol', 'thalach', 'ca', 'outcome']]
    X = np.array(df.drop(['outcome'], 1))
    y = np.array(df['outcome'])
    X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=0.2)
    clf = svm.SVC()
    clf.fit(X_train, y_train)
    confidence = clf.score(X_test, y_test)
    example_measures = np.array([[57, 1, 4, 192, 148, 0]])
    example_measures = example_measures.reshape(len(example_measures), -1)
    prediction = clf.predict(example_measures)
    return HttpResponse(prediction)
