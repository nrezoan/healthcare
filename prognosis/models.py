from django.db import models


# Create your models here.

class AttributeTable(models.Model):
    age = models.IntegerField(verbose_name="Age")
    sex = models.BooleanField(verbose_name="Check Means Male Unchecked Means Female")
    cp = models.IntegerField(verbose_name="Chest")
    chol = models.IntegerField(verbose_name="Cholesterol")
    thalach = models.IntegerField(verbose_name="Max Heart Rate")
    ca = models.IntegerField(verbose_name="Number Of Major Vessels")
    outcome = models.IntegerField(verbose_name="The Predicted Attribute")
